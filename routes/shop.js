const path = require('path');

const express = require('express');

const ShopController = require('../controllers/shop');

const router = express.Router();

router.get('/', ShopController.getIndex);

router.get('/products', ShopController.getProducts);

router.get('/products/:Id', ShopController.getProduct);

router.post('/cart', ShopController.postCart);
/*
router.get('/cart', ShopController.getCart);



router.post('/cart-delete-item', ShopController.postCartDeleteProduct);

router.post('/create-order', ShopController.postOrder);

router.get('/orders', ShopController.getOrders);
*/

//router.get('/chekout', ShopController.getCheckout);

module.exports = router;
