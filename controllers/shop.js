const Product = require('../models/products');
const Order = require('../models/order');


exports.getProducts = (req, res, next) => {
    Product.fetchAll().then(products => {
        res.render('shop/product-list', {
            prods: products,
            pageTitle: 'All products',
            path: '/products',
            hasProducts: products.length > 0,
            activeShop: true,
            productCSS: true
        });
    }).catch(err => {
        console.log(err);
    });
    /*    Product.fetchAll().then(([rows, fieldData]) => {
            res.render('shop/product-list', {
                prods: rows,
                pageTitle: 'All products',
                path: '/products',
                hasProducts: rows.length > 0,
                activeShop: true,
                productCSS: true
            });
        })
            .catch(err => console.log(err));*/

};
exports.getProduct = (req, res, next) => {
    const prodId = req.params.Id;
    Product.findById(prodId).then(product => {
        res.render('shop/product-detail', {
            product: product,
            pageTitle: product.title,
            path: '/products'
        });
    })
        .catch(err => console.log(err));
};
exports.getIndex = (req, res, next) => {
    Product.fetchAll().then(products => {
        res.render('shop/index', {
            prods: products,
            pageTitle: 'Shop',
            path: '/',
        });
    }).catch(err => {
        console.log(err);
    });
    /*    Product.fetchAll()
            .then(([rows, fieldData]) => {
                res.render('shop/index', {
                    prods: rows,
                    pageTitle: 'Shop',
                    path: '/',
                });
            })
            .catch(err => console.log(err));*/
};
exports.getCart = (req, res, next) => {
    req.user.getCart().then(
        cart => {
            return cart.getProducts().then(products => {
                res.render('shop/cart', {
                    path: '/cart',
                    pageTitle: 'Your cart',
                    products: products
                });
            }).catch(err => {
                console.log(err);
            });
        }
    ).catch(err => {
        console.log(err);
    });
    /*    Cart.getCart(cart => {
            Product.fetchAll(products => {
                const cartProducts = [];
                for (product of products) {
                    const cartProductData = cart.products.find(prod => prod.id === product.id);
                    if (cartProductData) {
                        cartProducts.push({productData: product, qty: cartProductData.qty});
                    }
                }
                res.render('shop/cart', {
                    path: '/cart',
                    pageTitle: 'Your cart',
                    products: cartProducts
                });
            });
        });*/
};


exports.postCart = (req, res, next) => {
    const prodId = req.body.productID;
    Product.findById(prodId).then(product => {
        return req.user.addToCart(product);
    }).then(result => {
        console.log(result);
        res.redirect('/cart');
    });
    /*    let fetchedCart;
        let newQuantity = 1;
        req.user
            .getCart()
            .then(cart => {
                fetchedCart = cart;
                return cart.getProducts({where: {id: prodId}});
            })
            .then(products => {
                let product;
                if (products.length > 0) {
                    product = products[0];
                }

                if (product) {
                    const oldQuantity = product.cartItem.quantity;
                    newQuantity = oldQuantity + 1;
                    return product;
                }
                return Product.findByPk(prodId);
            })
            .then(product => {
                return fetchedCart.addProduct(product, {
                    through: {quantity: newQuantity}
                });
            })
            .then(() => {
                res.redirect('/cart');
            })
            .catch(err => console.log(err));
        /!*    console.log(prodId);
            Product.findById(prodId, (product) => {
                Cart.addProduct(prodId, product.price);
            });
            res.redirect('/cart');*!/*/
};
exports.postCartDeleteProduct = (req, res, next) => {
    const prodId = req.body.productID;
    req.user.getCart()
        .then(cart => {
            return cart.getProducts({where: {id: prodId}});
        })
        .then(products => {
            const product = products[0];
            return product.cartItem.destroy();
        })
        .then(result => {
            res.redirect('/cart');
        })
        .catch(err => console.log(err));
    /*
    Product.findById(prodId, p => {
        Cart.deleteProduct(prodId, p.price);
        res.redirect('/cart');
    });*/

};
exports.getOrders = (req, res, next) => {
    req.user.getOrders({include: ['products']})
        .then(orders => {
            res.render('shop/orders', {
                path: '/orders',
                pageTitle: 'Your orders',
                orders: orders
            });
        })
        .catch(err => console.log(err));

};
exports.getCheckout = (req, res, next) => {

    res.render('shop/checkout', {
        pageTitle: 'checkout',
        path: '/checkout'
    });

};

exports.postOrder = (req, res, next) => {
    let fetchedCart;
    req.user.getCart()
        .then(cart => {
            fetchedCart = cart;
            return cart.getProducts();
        })
        .then(products => {
            return req.user.createOrder()
                .then(order => {
                    order.addProducts(products.map(product => {
                        product.orderItem = {quantity: product.cartItem.quantity};
                        return product;
                    }))
                }).catch(err => console.log(err));
        })
        .then(result => {
            fetchedCart.setProducts(null);
            res.redirect('/orders');
        })
        .then(result => {
            res.redirect('/orders');
        })
        .catch(err => console.log(err));
};
