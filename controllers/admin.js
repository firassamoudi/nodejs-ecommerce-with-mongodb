const Product = require('../models/products');
const mongodb = require('mongodb');
const ObjectId = mongodb.ObjectId;
exports.getAddProduct = (req, res, next) => {
    res.render('admin/edit-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        editing: false
    });
};
exports.postAddProduct = (req, res, next) => {
    const title = req.body.title;
    const imageUrl = req.body.imageUrl;
    const price = req.body.price;
    const description = req.body.description;
    const product = new Product(title, imageUrl, price, description, null,req.user._id);
    product.save()
        .then(result => {
            console.log('Created product');
            res.redirect('/admin/product');
        })
        .catch(err => {
            console.log(err);
        });

};
exports.getProducts = (req, res, next) => {
    Product.fetchAll()
        .then(products => {
            res.render('admin/products', {
                prods: products,
                pageTitle: 'Admin products',
                path: '/admin/product',
            });
        }).catch(err => {
        console.log(err);
    });
};
exports.getEditProduct = (req, res, next) => {
    const editMode = req.query.edit;
    if (!editMode) {
        return res.redirect('/');
    }
    const prodId = req.params.productId;
    Product.findById(prodId)
        .then(product => {
            if (!product) {
                return res.redirect('/');
            }
            res.render('admin/edit-product', {
                pageTitle: 'Edit Product',
                path: '/admin/edit-product',
                editing: editMode,
                product: product
            });
        }).catch(err => {
        console.log(err);
    })

};

exports.postEditProduct = (req, res, next) => {
    const prodId = req.body.prodID;
    const updatedTitle = req.body.title;
    const updatedPrice = req.body.price;
    const updatedImageUrl = req.body.imageUrl;
    const updatedDescirption = req.body.description;

    const product = new Product(updatedTitle, updatedPrice, updatedImageUrl, updatedDescirption, new ObjectId(prodId));
    product.save()
        .then(productData => {
            return product.save();
        }).then(result => {
        console.log('updated');
        res.redirect('/admin/product');
    })
        .catch(err => {
            console.log(err);
        })
};
exports.postDeleteProduct = (req, res, next) => {
    const prodId = req.body.prodID;
    Product.deleteById(prodId).then(result => {
        console.log('destroyed product');
        res.redirect('/admin/product');
    }).catch(err => console.log(err));

};
