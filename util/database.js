const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;
const mongoConnect = (callback) => {
    MongoClient.connect('mongodb+srv://firas:firas123@@@cluster0-c9cjg.mongodb.net/test?retryWrites=true&w=majority')
        .then(client => {
            console.log('Connected');
            _db = client.db();
            callback();
        })
        .catch(err => {
            console.log(err);
            throw err;
        });
};
const getDb = () => {
    if (_db) {
        return _db;
    }
    throw 'no databse found';
}
exports.mongoConnect = mongoConnect;
exports.getDb = getDb;