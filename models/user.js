const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

class User {
    constructor(username, email, cart, id) {
        this.name = username;
        this.email = email;
        this.cart = cart;
        this._id = id;
    }

    save() {
        const db = getDb();
        return db.collection('Users').insertOne(this);
    }

    addToCart(product) {
        const cartProductIndex = this.cart.items.findIndex(cp => {
            return cp.productId.toString() === product._id.toString();
        });
        let newQuantity = 1;
        const updatedCartItems = [...this.cart.items];

        if (cartProductIndex >= 0) {
            newQuantity = this.cart.items[cartProductIndex].quantity + 1;
            updatedCartItems[cartProductIndex].quantity = newQuantity;
        } else {
            updatedCartItems.push({
                productId: new mongodb.ObjectId(product._id), quantity: newQuantity
            });
        }


        const updateCart = {
            items: updatedCartItems
        };
        const db = getDb();
        return db.collection('Users')
            .updateOne(
                {_id: new Object(this._id)},
                {$set: {cart: updateCart}});
    }

    static findById(userId) {
        const db = getDb();
        return db.collection('Users')
            .find({_id: new mongodb.ObjectId(userId)}).next()
            .then(user => {
                return user;
            })
            .catch(err => console.log(err));
    }
}


module.exports = User;