/*
//const fs = require('fs');
//const path = require('path');
const Cart = require('./cart');
const db = require('../util/database');
/!*const p = path.join(path.dirname(process.mainModule.filename),
    'data',
    'product.json');*!/
/!*
const getProductsFromFile = cb => {
    fs.readFile(p, (err, fileContent) => {
        if (err) {
            cb([]);
        } else {
            cb(JSON.parse(fileContent));
        }

    });
};*!/

module.exports = class Product {

    constructor(id, title, imageUrl, description, price) {
        this.id = id;
        this.title = title;
        this.imageUrl = imageUrl;
        this.description = description;
        this.price = price;
    }

    save() {
        return db.execute('insert into products (title, price,description,imageUrl) values (?,?,?,?)',
            [this.title, this.price, this.description, this.imageUrl]);

        /!*        getProductsFromFile(products => {
                    if (this.id) {
                        const existingProductIndex = products.findIndex(prod => prod.id === this.id);
                        const updateProducts = [...products];
                        updateProducts[existingProductIndex] = this;
                        fs.writeFile(p, JSON.stringify(updateProducts), (err) => {
                            console.log(err);
                        });
                    }
                    this.id = Math.random().toString();
                    products.push(this);
                    fs.writeFile(p, JSON.stringify(products), (err) => {
                        console.log(err);
                    });
                });*!/
    };

    static fetchAll() {
        // getProductsFromFile(cb);
        return db.execute('select * from products').then().catch();
    };

    static findById(id) {
        return db.execute('select * from products where products.id = ?', [id]);
        /!*        getProductsFromFile(products => {
                    const product = products.find(p => p.id === id);
                    cb(product);
                });*!/
    };

    static deletByid(id) {
        /!*        getProductsFromFile(products => {
                    const product = products.find(prod => prod.id ===id);
                    const updatedProducts = products.filter(p => p.id !== id);
                    fs.writeFile(p, JSON.stringify(updatedProducts), (err) => {
                        if (!err) {
                            Cart.addProduct(id,product.price);
                        }
                    });
                });*!/
    };
}*/

const getDb = require('../util/database').getDb;
const mongodb = require('mongodb');

class Product {
    constructor(title, price, imageUrl, description, id, userId) {
        this.title = title;
        this.price = price;
        this.imageUrl = imageUrl;
        this.description = description;
        this._id = id ? new mongodb.ObjectId(id) : null;
        this.userId = userId;
    }

    save() {
        const db = getDb();
        let dbOp;
        if (this._id) {
            dbOp = db.collection('products').updateOne({_id: new mongodb.ObjectId(this._id)}, {$set: this});
        } else {
            dbOp = db.collection('products').insertOne(this);
        }

        return dbOp
            .then(result => {
                console.log(result)
            })
            .catch(err => console.log(err));
    };

    static fetchAll() {
        const db = getDb();
        return db.collection('products')
            .find().toArray()
            .then(products => {
                return products;
            })
            .catch(err => console.log(err));
    }

    static findById(id) {
        const db = getDb();
        return db.collection('products')
            .find({_id: new mongodb.ObjectId(id)}).next()
            .then(product => {
                return product;
            })
            .catch(err => console.log(err));
    }

    static deleteById(id) {
        const db = getDb();
        return db.collection('products')
            .deleteOne({_id: new mongodb.ObjectId(id)})
            .then(product => {
                console.log('deleted');
            })
            .catch(err => console.log(err));
    }
}


module.exports = Product;